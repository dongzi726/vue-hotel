package com.hotel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("type_room")
public class TypeRoom {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String typeName;
    private String typePrice;
    private Integer typeNum;
    private String cover;
    private String typeDetail;

    @TableField(exist = false)
    private Integer countValue;
}
