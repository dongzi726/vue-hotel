package com.hotel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
/*指定数据库名字*/
@TableName("news")
public class News {
    /*指定主键的方式*/
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String title;
    private String content;
    /*@JsonFormat可以很好的解决：后台到前台时间格式保持一致的问题*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;

}
