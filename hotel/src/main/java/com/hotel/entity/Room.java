package com.hotel.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
/*无参构造*/
@NoArgsConstructor
/*全参构造*/
@AllArgsConstructor
@TableName("room")
public class Room {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer typeId;
    private Integer homeStatus;
    private String remarks;
    private String area;
    private Integer bedNum;
    private double bedSize;


}
