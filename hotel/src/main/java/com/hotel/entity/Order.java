package com.hotel.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("t_order")
/*无参构造*/
@NoArgsConstructor
/*全参构造*/
@AllArgsConstructor
public class Order {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;
    private BigDecimal roomPrice;
    private BigDecimal payPrice;
    private String orderNo;
    private Integer roomId;
    private Integer userId;
    private String username;
    private Integer orderStatus;
    private Integer day;
    private String phone;
    private String tyName;

}
