package com.hotel.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomAll {
    private Integer id;
    private String typeName;
    private String typePrice;
    private Integer typeNum;
    private String cover;
    private String typeDetail;
    private String remarks;
    private Integer homeStatus;
    private String area;
    private Integer bedNum;
    private double bedSize;


}
