package com.hotel.controller;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hotel.controller.dto.AliPay;
import com.hotel.entity.Order;
import com.hotel.entity.Room;
import com.hotel.mapper.OrderMapper;
import com.hotel.mapper.RoomMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/alipay")
public class AliPayController {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    RoomMapper roomMapper;

    @GetMapping("/pay")
    public String pay(AliPay aliPay) {
        AlipayTradePagePayResponse response;
        try {
            //  发起API调用（以创建当面付收款二维码为例）
            response = Factory.Payment.Page()
                    .pay(aliPay.getSubject(),
                            aliPay.getTraceNo(),
                            aliPay.getTotalAmount(), "");
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return response.getBody();
    }


    @PostMapping("/notify")  // 注意这里必须是POST接口
    public String payNotify(HttpServletRequest request) throws Exception {

        /*
        * 我的沙箱账号 cfxqmq6286@sandbox.com
        * */
        if (request.getParameter("trade_status").equals("TRADE_SUCCESS")) {
            System.out.println("=========支付宝异步回调========");
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (String name : requestParams.keySet()) {
                params.put(name, request.getParameter(name));
            }
            /*这个就是自己的订单号*/
            String tradeNo = params.get("out_trade_no");
            // 支付宝验签
            if (Factory.Payment.Common().verifyNotify(params)) {
                // 验签通过
                System.out.println("交易名称: " + params.get("subject"));
                System.out.println("交易状态: " + params.get("trade_status"));
                System.out.println("支付宝交易凭证号: " + params.get("trade_no"));
                System.out.println("商户订单号: " + params.get("out_trade_no"));
                System.out.println("交易金额: " + params.get("total_amount"));
                System.out.println("买家在支付宝唯一id: " + params.get("buyer_id"));
                System.out.println("买家付款时间: " + params.get("gmt_payment"));
                System.out.println("买家付款金额: " + params.get("buyer_pay_amount"));
                /*获得根据订单号查询订单*/
                QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
                orderWrapper.eq("order_no",tradeNo);
                /*获得当前订单*/
                Order order = orderMapper.selectOne(orderWrapper);
                /*获得当前订单信息的房间号*/
                Integer roomId = order.getRoomId();
                /*获得房间信息*/
                Room room = roomMapper.selectById(roomId);
                /*修改订单状态*/
                orderMapper.updateState(tradeNo, 1);
                /*修改房间状态*/
                room.setHomeStatus(1);
                room.setId(roomId);
                /*更新房间信息*/
                roomMapper.updateById(room);
            }
        }
        return "success";
    }

}
