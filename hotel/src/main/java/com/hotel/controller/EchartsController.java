package com.hotel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hotel.common.Result;
import com.hotel.entity.Room;
import com.hotel.entity.TypeRoom;
import com.hotel.mapper.HomeCategoryMapper;
import com.hotel.mapper.OrderMapper;
import com.hotel.mapper.RoomMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/echarts")
public class EchartsController {

    @Resource
    private RoomMapper roomMapper;

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private HomeCategoryMapper homeCategoryMapper;
    //获得房间总数
    @GetMapping("/getCountRooms")
    public Result<?> getAllRoomNum(){
        Integer integer = roomMapper.selectCount(null);
        return Result.success(integer);
    }


    //今日房间已入住的数量
    @GetMapping("/rentRooms")
    public Result<?> rentRoomNum(){
        QueryWrapper<Room> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("home_status","1");
        Integer integer = roomMapper.selectCount(queryWrapper);
        return Result.success(integer);
    }

    //今日空闲房间的数量
    @GetMapping("/vacantRooms")
    public Result<?> vacantRoomNum(){
        QueryWrapper<Room> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("home_status","0");
        Integer integer = roomMapper.selectCount(queryWrapper);
        return Result.success(integer);
    }

    //收益的总金额
    @GetMapping("/getAllMoney")
    public Result<?> getAllMoney(){
        int sumMoney = orderMapper.selectAllMoney();
        return Result.success(sumMoney);

    }

    //图表的数据  房间的类型和对应类型的数量
    @GetMapping("/getMembers")
    public Result<?> getMembers(){
        List<TypeRoom> typeRoomList = homeCategoryMapper.queryCharts();
        return Result.success(typeRoomList);
    }





    //房间已经入住和空房间的数量






}
