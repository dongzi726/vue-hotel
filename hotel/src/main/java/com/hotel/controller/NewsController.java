package com.hotel.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hotel.common.Result;
import com.hotel.entity.News;
import com.hotel.mapper.NewsMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Resource
    NewsMapper newsMapper;

    /*前台接口*/

    /**
     * 查询所有新闻
     *
     * @return
     */
    @GetMapping
    public Result<?> findAllNews() {
        List<News> newsList = newsMapper.selectList(null);
        return Result.success(newsList);
    }

    /**
     * 添加数据
     * @param news
     * @return
     */
    @PostMapping
    public Result<?> insertNews(@RequestBody News news) {
        news.setTime(new Date());
        newsMapper.insert(news);
        return Result.success();
    }

    /**
     * 修改数据
     * @param news
     * @return
     */
    @PutMapping
    public Result<?> updateNew(@RequestBody News news) {
        newsMapper.updateById(news);
        return Result.success();
    }

    /**
     * 根据单个id删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> deleteById(@PathVariable Long id) {
        newsMapper.deleteById(id);
        return Result.success();
    }

    /*批量删除要用post*/
    @PostMapping("/deleteBatchs")
    public Result<?> deleteByIds(@RequestBody List<Integer> ids){
        newsMapper.deleteBatchIds(ids);
        return Result.success();

    }

    /**
     * 根据单个id进行查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<?> getById(@PathVariable Long id) {
        return Result.success(newsMapper.selectById(id));
    }


    /*分页查询所有新闻*/
    @GetMapping("/page")
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search) {
        LambdaQueryWrapper<News> wrapper = Wrappers.<News>lambdaQuery();
        if (StrUtil.isNotBlank(search)) {
            wrapper.like(News::getTitle, search);
        }
        Page<News> newsPage = newsMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(newsPage);
    }

}
