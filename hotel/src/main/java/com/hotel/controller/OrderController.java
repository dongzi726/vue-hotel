package com.hotel.controller;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hotel.common.Result;
import com.hotel.entity.Order;
import com.hotel.entity.Room;
import com.hotel.entity.User;
import com.hotel.mapper.OrderMapper;
import com.hotel.mapper.RoomMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController extends BaseController{

    @Resource
    OrderMapper orderMapper;

    @Resource
    RoomMapper roomMapper;


    /**
     * 前台客户生成订单
     *
     * @param order
     * @return
     */
    @PostMapping
    public Result<?> insertOrder(@RequestBody Order order) {
        User user=getUser();
        Integer id = user.getId();
        String username = user.getUsername();
        order.setUserId(id);
        order.setUsername(username);
        /*生成订单编号*/
        String orderNo = IdUtil.getSnowflake().nextIdStr();
        order.setOrderNo(orderNo);
        order.setOrderStatus(0);
        String productCode = IdUtil.getSnowflake().nextIdStr();
        String outTradeNo = IdUtil.getSnowflake().nextIdStr();
        /*
        * traceNo传递的是订单编号
        * 这有几个是唯一标识，而且都要唯一标识，所以要随机生成
        * */
        /*返回的数据url，就是调用支付宝的接口*/
        String payUrl = "http://localhost:9090/alipay/pay?subject=" +order.getTyName()
        + "&outTradeNo="+ outTradeNo +
        "&productCode=" + productCode +
        "&traceNo=" + orderNo +
        "&totalAmount=" +order.getPayPrice();
        /*先插入数据在订单数据库中*/
        orderMapper.insert(order);
        return Result.success(payUrl);
    }


    /**
     * 前台客户退房
     *
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public Result<?> updateBId(@PathVariable("id") Integer id) {

        /*id查询订单获得退房人的订单*/
        Order order = orderMapper.selectById(id);
        /*修改订单状态为以退房*/
        order.setOrderStatus(2);
        /*修改订单状态*/
        orderMapper.updateById(order);
        /*获得房间信息*/
        Integer roomId = Integer.valueOf(order.getRoomId());
        /*跟据房间id查询房间信息*/
        Room room = roomMapper.selectById(roomId);
        /*修改房间信息为以退房状态*/
        room.setHomeStatus(0);
        /*跟新房间状态信息*/
        roomMapper.updateById(room);
        return Result.success();
    }


    /**
     * 前台客户查询所有订单
     *
     * @param
     * @return
     */
    @GetMapping("/orderdata")
    public Result<?> getOrderById() {
        /*根据token获得user对象*/
        User user=getUser();
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",user.getId());
        /*获得所有user_id为 userId的订单信息*/
        List<Order> orders = orderMapper.selectList(wrapper);
        return Result.success(orders);
    }


    @GetMapping("/timesorder/{id}")
    public Result<?> getOrderByLoad(@PathVariable("id") Integer id) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",id);
        /*获得所有user_id为 userId的订单信息*/
        List<Order> orders = orderMapper.selectList(wrapper);
        return Result.success(orders);
    }


    /**
     * 管理员修改订单状态和付款金额
     *
     * @param order
     * @return
     */
    @PutMapping
    public Result<?> updataOrder(@RequestBody Order order) {
        orderMapper.updateById(order);
        return Result.success();
    }


    /**
     * 根据单个id进行删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> deleteCarousel(@PathVariable Integer id) {
        orderMapper.deleteById(id);
        return Result.success();
    }

    /**
     * 多个id进行删除
     *
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatchs")
    public Result<?> deleteCarousel(@RequestBody List<Integer> ids) {
        orderMapper.deleteBatchIds(ids);
        return Result.success();
    }

    @GetMapping("/adminorder")
    public Result<?> queyAdminOrder(Integer pageNum,
                                    Integer pageSize,
                                    @RequestParam(defaultValue = "") String search

    ) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(search)) {
            wrapper.like("order_no", search);
        }
        Page<Order> orderPage = orderMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(orderPage);

    }
}