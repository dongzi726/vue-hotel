package com.hotel.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hotel.common.Result;
import com.hotel.entity.TypeRoom;
import com.hotel.mapper.HomeCategoryMapper;
import com.hotel.service.HomeCategoryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/homecategory")
public class HomeCategoryController {
    @Resource
    private HomeCategoryService homeCategoryService;
    @Resource
    private HomeCategoryMapper homeCategoryMapper;

    /*查询该酒店所有的种类信息，展示在用户前台处*/
    @GetMapping("/category")
    public Result<?> getAllCategory() {
        List<TypeRoom> list = homeCategoryService.queryCategory();
        return Result.success(list);

    }

    /*根据搜索框搜素进行模糊查询*/
    @GetMapping("/selectByLike")
    public Result<?> selectAllByLike(Integer pageNum,
                                     Integer pageSize,
                                     @RequestParam(defaultValue = "") String search
    ) {
        QueryWrapper<TypeRoom> wrapper = new QueryWrapper<>();
        /*StrUtil.isNotBlank(search)判断某字符串是否不为空且长度不为0且不由空白符构成,hutool工具类里面的*/
        if (StrUtil.isNotBlank(search)) {
            wrapper.like("type_name", search);
        }
        Page<TypeRoom> typeRoomPage = homeCategoryMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(typeRoomPage);
    }

    /*新增客房种类*/
    @PostMapping
    public Result<?> insertCategory(@RequestBody TypeRoom typeRoom) {
        /*获取新增类型名*/
        String typeName = typeRoom.getTypeName();
        /*从数据库查询，看看是否已经有此类型了*/
        QueryWrapper<TypeRoom> wrapper = new QueryWrapper<>();
        wrapper.eq("type_name", typeName);
        TypeRoom typeRoom1 = homeCategoryMapper.selectOne(wrapper);
        if (typeRoom1==null){
            homeCategoryMapper.insert(typeRoom);
            return Result.success();
        }else {
            return Result.error("300", "已经有此类型房间了");
        }
    }

    /*修改客房类型*/
    @PutMapping("/updata")
    public Result<?> updataCategory(@RequestBody TypeRoom typeRoom) {
        homeCategoryMapper.updateById(typeRoom);
        return Result.success();

    }


    /*删除，单个删除*/
    @DeleteMapping("/{id}")
    public Result<?> deleteById(@PathVariable Integer id) {
        homeCategoryMapper.deleteById(id);
        return Result.success();
    }

    /*postman测试接口的时候，选择body  数据格式json，参数写["9","10"]*/
    /*根据多个id循环删除*/
    @PostMapping("/deleteBatchs")
    public Result<?> deleteByIds(@RequestBody List<Integer> ids) {
        homeCategoryMapper.deleteBatchIds(ids);
        return Result.success();

    }


}
