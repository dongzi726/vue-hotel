package com.hotel.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.hotel.common.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

@RestController
@RequestMapping("/files")
public class FileController {
    @Value("9090")
    private String port;

    @Value("localhost")
    private String ip;

    public static final String BASE_PATH = "E:/毕业设计/hotel/files/";

    /*上传接口*/
    @PostMapping("/upload")
    public Result<?> upload(MultipartFile file) throws IOException {
        if (file == null) {
            return null;
        }
        /*获取当前时间,作为图片的唯一标识*/
        long flag = System.currentTimeMillis();
        //文件存储的路径
        File savePath = new File(BASE_PATH + flag + file.getOriginalFilename());
        System.out.println(savePath);
        /*判断如果父目录不存在，则创建一个*/
        if (!savePath.getParentFile().exists()) {
            savePath.getParentFile().mkdirs();
        }
        //进行文件存储
        file.transferTo(savePath);
        /*返回url*/
        return Result.success("http://"+ip+":"+port+"/files/" + flag);
    }


    /*因为富文本编辑器wangeditor官网自己要求的有json格式的返回方式，所以要重新写后台处理图片的接口*/
    @PostMapping("/editor/upload")
    public JSON editorUpload(MultipartFile file) throws IOException {

        /*获取原文件的名字*/
        String originalFileName= file.getOriginalFilename();
        /*定义唯一标识*/
        String flag= IdUtil.fastSimpleUUID();
        /*上传路径*/
        String path="E:/毕业设计/hotel/files/"+flag+"_"+originalFileName;
        /*将其写入该路径中*/
        FileUtil.writeBytes(file.getBytes(),path);

        String url = "http://" + ip + ":" + port + "/files/" + flag;

        /*富文本编辑器要求的格式*/
        JSONObject jsonObject=new JSONObject();
        jsonObject.set("errno",0);

        JSONArray array=new JSONArray();
        JSONObject data=new JSONObject();

        data.set("url",url);
        array.add(data);

        jsonObject.set("data",array);
        return jsonObject;
    }

    /*文件下载*/
    @GetMapping("/{flag}")
    public void getFiles(@PathVariable String flag, HttpServletResponse response) {
        /*新建一个输出流对象*/
        OutputStream os;
        /*获得文件上传的路径*/
        String path = BASE_PATH;
        /*获得路径下的所有文件名*/
        List<String> fileNames = FileUtil.listFileNames(path);
        /*找到跟参数一样名称的文件*/
        String fileName = fileNames.stream().filter(name -> name.contains(flag)).findAny().orElse("");
        try {
            if (StrUtil.isNotEmpty(fileName)) {
                /*设置文件响应头*/
                response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
                response.setContentType("application/octet-stream");
                /*将文件以二进制读出来*/
                byte[] bytes = FileUtil.readBytes(path + fileName);
                // 通过输出流返回文件
                os = response.getOutputStream();
                os.write(bytes);
                os.flush();
                os.close();
            }
        } catch (IOException e) {
            System.out.println("文件下载失败");
        }
    }
}

