package com.hotel.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hotel.common.Result;
import com.hotel.entity.Carousel;
import com.hotel.mapper.CarouselMapper;
import com.hotel.service.CarouselService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/carousel")
public class CarouselController {
    @Resource
    private CarouselService carouselService;

    @Resource
    private CarouselMapper carouselMapper;

    /**
     * 前台展示轮播图
     *
     * @return
     */
    @GetMapping
    public Result<?> queryAllImg(){
        List<?> list=carouselService.queryAll();
        return Result.success(list);
    }

    /**
     * 增加数据
     *
     * @param carousel
     * @return
     */
    @PostMapping
    public Result<?> insertCarousel(@RequestBody Carousel carousel) {
        carouselMapper.insert(carousel);
        return Result.success();
    }

    /**
     * 修改图片
     *
     * @param carousel
     * @return
     */
    @PutMapping
    public Result<?> updataCarousel(@RequestBody Carousel carousel) {
        carouselMapper.updateById(carousel);
        return Result.success();
    }

    /**
     * 根据单个id进行删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> deleteCarousel(@PathVariable Integer id) {
        carouselMapper.deleteById(id);
        return Result.success();
    }

    /**
     * 多个id进行删除
     *
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatchs")
    public Result<?> deleteCarousel(@RequestBody List<Integer> ids) {
        carouselMapper.deleteBatchIds(ids);
        return Result.success();
    }

    @GetMapping("/admincarousel")
    public Result<?> queyAdminImg(Integer pageNum,
                                     Integer pageSize
    ) {
        Page<Carousel> carouselPage = carouselMapper.selectPage(new Page<>(pageNum,pageSize), null);
        return Result.success(carouselPage);
    }






}
