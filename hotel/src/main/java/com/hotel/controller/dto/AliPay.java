package com.hotel.controller.dto;

import lombok.Data;

@Data
public class AliPay {

    /*跳转到支付接口传递的属性*/
    private String subject;
    private String traceNo;
    private String totalAmount;
}
