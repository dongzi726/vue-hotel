package com.hotel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.hotel.common.Result;
import com.hotel.entity.Room;
import com.hotel.entity.TypeRoom;
import com.hotel.entity.vo.RoomAll;
import com.hotel.mapper.HomeCategoryMapper;
import com.hotel.mapper.RoomMapper;
import com.hotel.service.RoomService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Resource
    private RoomService roomService;

    @Resource
    private RoomMapper roomMapper;

    @Resource
    private HomeCategoryMapper homeCategoryMapper;
    /*前台接口*/

    /**
     * ? 通配符
     * 查询所有房间信息，双表联查,type_room和room两表联查
     *
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping
    public Result<?> getAllRoom(Integer pageNum,
                                Integer pageSize,
                                @RequestParam(defaultValue = "") String name
    ) {
        PageInfo<RoomAll> list = roomService.queryRoomInfoAll(pageNum, pageSize, name);
        return Result.success(list);
    }

    /*管理员后台接口*/

    /**
     * 根据搜索框搜素进行模糊查询
     *
     * @param pageNum
     * @param pageSize
     * @param search
     * @return
     */
    @GetMapping("/selectByLike")
    public Result<?> selectAllByLike(Integer pageNum,
                                     Integer pageSize,
                                     @RequestParam(defaultValue = "") String search
    ) {
         PageInfo<RoomAll> list = roomService.queryRoomAll(pageNum, pageSize, search);
        return Result.success(list);
    }


    /**
     * 增加数据
     *
     * @param room
     * @return
     */
    @PostMapping
    public Result<?> insertRoom(@RequestBody Room room) {
        /*获得房间类型的id*/
        Integer typeId=room.getTypeId();
        /*查询是否有该类型客房*/
        QueryWrapper<TypeRoom> wrapper=new QueryWrapper<>();
        wrapper.eq("id",typeId);
        /*查询出数据*/
        TypeRoom typeRoom = homeCategoryMapper.selectOne(wrapper);
        /*如果查询结果为空,返回错误码*/
        if (typeRoom==null){
            return Result.error("400","该类型房间不存在!!");
        }else {
            /*如果该类型客房存在*/
            Integer typeNum = typeRoom.getTypeNum();
            /*给该类型客房数量加1*/
            typeRoom.setTypeNum(typeNum + 1);
            /*插入新增房间*/
            roomMapper.insert(room);
            /*更新类别数量*/
            homeCategoryMapper.updateById(typeRoom);
            return Result.success();
        }
    }

    /**
     * 修改房间信息
     *
     * @param room
     * @return
     */
    @PutMapping("/updata")
    public Result<?> updataRoom(@RequestBody Room room) {
        roomMapper.updateById(room);
        return Result.success();
    }

    /**
     * 根据单个id进行删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> deleteRoom(@PathVariable Integer id) {
        QueryWrapper<Room> wrapper=new QueryWrapper<>();

        wrapper.eq("id",id);
        Room room = roomMapper.selectOne(wrapper);
        Integer typeId=room.getTypeId();
        /*根据id查询该房间*/
        QueryWrapper<TypeRoom> wrapper1=new QueryWrapper<>();
        wrapper1.eq("id",typeId);
        /*查询出数据*/
        TypeRoom typeRoom = homeCategoryMapper.selectOne(wrapper1);
        /*获得类型数量*/
        Integer typeNum = typeRoom.getTypeNum();
        /*给该类型客房数量加1*/
        typeRoom.setTypeNum(typeNum-1);
        /*更新类别数量*/
        homeCategoryMapper.updateById(typeRoom);
        /*删除房间*/
        roomMapper.deleteById(id);
        return Result.success();

    }

    /**
     * 多个id进行删除
     *
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatchs")
    public Result<?> deleteRoom(@RequestBody List<Integer> ids) {
        for (Integer id:ids
             ) {
            QueryWrapper<Room> wrapper = new QueryWrapper<>();
            wrapper.eq("id", id);
            Room room = roomMapper.selectOne(wrapper);
            /*获得房间类型的id*/
            Integer typeId = room.getTypeId();
            /*根据id查询该房间*/
            QueryWrapper<TypeRoom> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("id", typeId);
            /*查询出数据*/
            TypeRoom typeRoom = homeCategoryMapper.selectOne(wrapper1);
            /*获得类型数量*/
            Integer typeNum = typeRoom.getTypeNum();
            /*给该类型客房数量加1*/
            typeRoom.setTypeNum(typeNum - 1);
            /*更新类别数量*/
            homeCategoryMapper.updateById(typeRoom);
            /*删除房间*/
            roomMapper.deleteById(id);
        }
        return Result.success();

    }


}
