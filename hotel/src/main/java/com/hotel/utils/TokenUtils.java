package com.hotel.utils;

import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.hotel.entity.User;
import com.hotel.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/*jwt生成token的工具类*/
public class TokenUtils {

    @Autowired
    private UserMapper userMapper;

    private static UserMapper staticUserMapper;

    /*只加载次，*/
    @PostConstruct
    public void init() {
        staticUserMapper = userMapper;
    }


    public static String getToken(User user){
        /*withAudience设置aud（受众群体）声明*/
        /*withExpiresAt设置token失效时间*/
        return JWT.create().withExpiresAt(DateUtil.offsetDay(new Date(),1))
                .withAudience(user.getId().toString()).sign(Algorithm.HMAC256(user.getPassword()));

    }

    public static User getUser(){
        /*获得request对象*/
       HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
       /*获得请求头获得token*/
        String token = request.getHeader("token");
        /*获得token里面的存的userId*/
        String aud = JWT.decode(token).getAudience().get(0);
        Integer userId=Integer.valueOf(aud);
        /*根据id查询user对象*/
        return staticUserMapper.selectById(userId);



    }


}
