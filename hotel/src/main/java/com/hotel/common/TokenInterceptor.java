package com.hotel.common;

import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import com.hotel.entity.User;
import com.hotel.exception.CustomException;
import com.hotel.mapper.UserMapper;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*验证token是否正确的拦截器*/
public class TokenInterceptor implements HandlerInterceptor {
    @Resource
    UserMapper userMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
     /*   *//*获得token从request请求中*//*
        String token = request.getHeader("token");
        *//*StringUtils.isEmpty和StringUtils.isBlank用法和区别
　　　　   两个方法都是判断字符是否为空的。前者是要求没有任何字符，
         即str==null 或 str.length()==0；后者要求是空白字符，
         即无意义字符。其实isBlank判断的空字符是包括了isEmpty的。
         换句话说，isEmpty判断的范围更小，只是在没有字符的情况下。*//*

        *//*判断token*//*
        if (StrUtil.isBlank(token)) {
            throw new CustomException("401", "未获取到token请重新登录");
        }
        *//*解析Token*//*
        DecodedJWT decode = JWT.decode(token);
        *//*因为我的生成token的时候，withAudience存储的是userId，所以我获取的也是userId*//*
        String userId = decode.getAudience().get(0);
        *//*根据userId获取用户信息*//*
        User user = userMapper.selectById(Integer.valueOf(userId));
        if (user == null) {
            throw new CustomException("401", "token不合法");
        }
        *//*开始验证token*//*
        Verification require = JWT.require(Algorithm.HMAC256(user.getPassword()));
        JWTVerifier build = require.build();
        try {
            build.verify(token);
        } catch (Exception e) {
            throw new CustomException("401", "token不合法");
        }
*/
        return true;
    }

}
