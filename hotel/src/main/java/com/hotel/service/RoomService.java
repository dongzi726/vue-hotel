package com.hotel.service;

import com.github.pagehelper.PageInfo;
import com.hotel.entity.vo.RoomAll;


public interface RoomService {

    /**
     * 查询所有记录
     * @return
     */
    PageInfo<RoomAll> queryRoomInfoAll(Integer pageNum, Integer limit, String name);

    PageInfo<RoomAll> queryRoomAll(Integer pageNum, Integer limit, String typeId);

}
