package com.hotel.service.serviceImpl;

import com.hotel.entity.TypeRoom;
import com.hotel.mapper.HomeCategoryMapper;
import com.hotel.service.HomeCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class HomeCategoryServiceImpl implements HomeCategoryService {
    @Resource
    private HomeCategoryMapper homeCategoryMapper;

    @Override
    public List<TypeRoom> queryCategory() {
        List<TypeRoom> list=homeCategoryMapper.queryCategory();
        return list;
    }
}
