package com.hotel.service.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hotel.entity.TypeRoom;
import com.hotel.entity.vo.RoomAll;
import com.hotel.mapper.HomeCategoryMapper;
import com.hotel.mapper.RoomMapper;
import com.hotel.service.HomeCategoryService;
import com.hotel.service.RoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class RoomServiceImpl implements RoomService {
    @Resource
    private RoomMapper roomMapper;

    @Override
    public PageInfo<RoomAll> queryRoomInfoAll(Integer pageNum, Integer pageSize,String name) {
        /**/
        /*进行分页*/
        PageHelper.startPage(pageNum,pageSize);
        List<RoomAll> results=roomMapper.queryRoomInfoAll(name);
        return new PageInfo<>(results);

    }

    @Override
    public PageInfo<RoomAll> queryRoomAll(Integer pageNum, Integer pageSize, String typeId) {
        PageHelper.startPage(pageNum,pageSize);
        List<RoomAll> results=roomMapper.queryRoomAll(typeId);
        return new PageInfo<>(results);
    }

}
