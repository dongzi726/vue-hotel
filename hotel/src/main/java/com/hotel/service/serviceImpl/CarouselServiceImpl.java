package com.hotel.service.serviceImpl;

import com.hotel.entity.Carousel;
import com.hotel.mapper.CarouselMapper;
import com.hotel.service.CarouselService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CarouselServiceImpl implements CarouselService {
    @Resource
    CarouselMapper carouselMapper;

    @Override
    public List<Carousel> queryAll() {
        return carouselMapper.queryAll();
    }
}
