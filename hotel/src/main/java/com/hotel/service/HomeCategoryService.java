package com.hotel.service;

import com.hotel.entity.TypeRoom;

import java.util.List;

public interface HomeCategoryService {

    List<TypeRoom> queryCategory();
}
