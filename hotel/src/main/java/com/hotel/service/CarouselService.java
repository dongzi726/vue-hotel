package com.hotel.service;

import com.hotel.entity.Carousel;

import java.util.List;

public interface CarouselService {
    List<Carousel> queryAll();
}
