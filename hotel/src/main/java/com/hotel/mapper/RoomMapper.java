package com.hotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.entity.Room;
import com.hotel.entity.vo.RoomAll;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoomMapper extends BaseMapper<Room> {
    /**
     *查询所有房间信息
     * @param name
     * @return
     */
    List<RoomAll> queryRoomInfoAll(@Param("name") String name);

    List<RoomAll> queryRoomAll(@Param("typeId") String typeId);

}
