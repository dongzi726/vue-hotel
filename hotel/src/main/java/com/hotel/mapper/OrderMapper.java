package com.hotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


public interface OrderMapper extends BaseMapper<Order> {

    @Update("update t_order set order_status = #{state} where order_no = #{tradeNo}")
    int updateState(@Param("tradeNo") String tradeNo, @Param("state") int state);

    @Select("select sum(pay_price) from t_order")
    int selectAllMoney();
}
