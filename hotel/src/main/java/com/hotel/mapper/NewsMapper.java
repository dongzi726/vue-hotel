package com.hotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.entity.News;

public interface NewsMapper extends BaseMapper<News> {
}
