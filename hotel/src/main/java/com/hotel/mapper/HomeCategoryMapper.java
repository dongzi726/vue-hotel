package com.hotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.entity.TypeRoom;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface HomeCategoryMapper extends BaseMapper<TypeRoom> {
    /**
     *查询房间所有种类
     * @return
     */
    List<TypeRoom> queryCategory();

    @Select("select type_room.type_name,count(type_id) as count_value from room left join type_room  on type_room.id=room.type_id GROUP BY room.type_id")
    List<TypeRoom> queryCharts();
}
