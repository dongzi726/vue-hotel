package com.hotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.entity.Carousel;

import java.util.List;

public interface CarouselMapper extends BaseMapper<Carousel> {

    /**
     *查询所有图片信息
     * @return
     */
    List<Carousel> queryAll();
}
